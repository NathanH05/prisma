# Challenge 3: Website Deployment

Welcome to Challenge 3.

This project was bootstrapped with [Create Next App](https://github.com/segmentio/create-next-app).

## Task 1

Given this project deploy it to AWS in an automated and reproducible fashion. The website should be reachable from all over the world.

__***Feedback:***__

See prisma-baking directory for ansible building of the AMI

See infra directory for ansible and terraform scripts that build using the pre-baked ami.

Stack colours used in naming conventions and tags to ensure a simple build with new stacks if required for HA or DR purposes.

## Task 2

Restrict access to the site by using mechanisms that can be adapted programmatically.

__***Feedback:***__

s3, object ACLs (Static sites only)

Deploying to lambda: Use of x-function keys to ensure only required components (API G/W) can trigger the function. 

NACLS can be used if hosting the service in AWS to allow selected client cidr blocks.

WAF - AWS blacklists certain sources regularly keeping your site protected from current known sources. Use of the following AWS WAF rule I recommended as an example of WAF applications and capabilities: AWSManagedRulesAmazonIpReputationList

Cloudfront distribution - country blacklists.

AWS security groups - simple to update the ip range in the variables for file (Ansible or Terraform). Can also have a SG for the load balancer that way the ec2 lb only allows traffic from the elb and no other sources



## Task 3

Deploy the site using at least 2 technology stacks. Make sure that both types of deployment can be reproduced in an automated fashion.

__***Feedback:***__

Terraform: see infra/terraform

Ansible - see infra/roles/infra 

## Task 4

What issues can you identify in the given site? What types of improvements would you suggest?

__***Feedback:***__


Data cleansing/extrapolation of the data recieved from ihavebeenpowned.com. Checks in the component js could be put in place to make sure what we are receiving is accurate and safe. e.g data type/length checks/other fields recieved from the api call.

It has no secure deployment.
Currently listens on port 3000, I suggest moving it to port 443 (HTTPS protocol) and issuing certificates with ACM or similar to ensure all traffic to and from the site is encrypted.

Renaming of the files to .js extensions

Store the daily data breaches in a dynamo DB or other DB. That way the application can monitor the data coming from the source and check it against the trends in the database. Would need to ensure protection against any db injection attacks.
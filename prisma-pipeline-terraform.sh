pipeline {
    agent any
    environment {
        PATH = "/usr/local/bin:$PATH"
    }
    stages {
        stage("Build infrastructure (Terraform)") {
            steps {
                dir('') {
                 git url: 'https://gitlab.com/NathanH05/prisma.git'
                 sh 'cd infra/terraform; terraform init; terraform apply --auto-approve'

                }
            }
        }
    }
}

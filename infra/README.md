# Prisma

An Ansible Playbook and Terraform scripts to create a single node behind a load balancer. The content of the site is distrubuted to sites across the world with CloudFront.


## Terraform cli
```terraform init```
```terraform apply```

## Ansible Parameters

### aws_region (mandatory)
The AWS region that we are accessing the AWS API's (as well as creating things in)

---

### tooling_environment (mandatory)
The tooling environment that is being used, e.g. `dev`, `qa`, `uat`, `prod`

### application_stack (optional)

---

Single node
```
---

- name: AWS Infrastructure
  hosts: localhost
  connection: local
  become: yes
  pre_tasks:
    - name: Validate parameters
      assert:
        that:
          - tooling_environment is defined
          - application_stack is defined
          - aws_region is defined
    - set_fact:
        single_instance: yes
  roles:
    - role: infra

  environment:
    http_proxy: "http://proxy.svpc.{{ (tooling_environment == 'prod') | ternary('prod', 'pre-prod' ) }}.eu-west-1.aws.internal:3128"
    https_proxy: "http://proxy.svpc.{{ (tooling_environment == 'prod') | ternary('prod', 'pre-prod' ) }}.eu-west-1.aws.internal:3128"
    no_proxy: "169.254.169.254,localhost"

```

This would normally result in three servers being created one each in the subnets `a`, `b` and `c`. However, due to free tier only one.


---


## CLI Usage

## Single node
```
ansible-playbook prisma.yml --extra-vars "tooling_environment=${var.env} application_stack=dev aws_region=eu-west-1"
```
---

note:
application_stack is optional. If not provided an available stack colour will be looked up

## Results
After the invocation of this role the following things will have happened:

* Baked AMI is looked up
* A singe node EC2 instance is spun up.
* Security group is configured for ec2 instance and ELB
* ELB is built
* Ansible tower gets installed


## Contributing

1. **Clone** the project to your own machine
2. **Commit** changes to your own branch
3. **Push** your work back up to your fork
4. Submit a **Pull request** so that we can review your changes

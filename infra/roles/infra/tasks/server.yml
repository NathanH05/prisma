---

- name: Set Server Facts
  set_fact:
    ec2_server_name: '{{ application_role_name }}'
    ec2_role_name: '{{ application_role_name }}'
    ec2_instance_type: '{{ servers.ec2_instance_type | default(ec2_instance_type_default) }}'

- debug:
    var: servers
- debug:
    var: application_stack

- name: Create EC2 hosts
  ec2:
    region: '{{ aws_region }}'
    key_name: '{{ aws_key_name }}'
    user_data: | 
      #!/bin/bash -xe 
      exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1 su ec2-user -c 'cd /home/ec2-user/prisma;curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash; . ~/.nvm/nvm.sh;nvm install node;npm install next;npm run build;npm run start'
    vpc_subnet_id: '{{ item.1.id }}'
    group_id: '{{ app_security_group.group_id }}'
    image: '{{ ec2_ami_id }}'
    instance_type: '{{ ec2_instance_type }}'
    #instance_profile_name: '{{ iam_instance_role }}'
    assign_public_ip: yes
    #ebs_optimized: '{{ servers.ebs_optimized | default(False) }}'
    #volumes: '{{ block_devices }}'
    instance_tags:
      Name: '{{ ec2_server_name }}'
      controlled_by: ansible
      created_at: '{{ ansible_date_time.iso8601 }}'
      tooling_environment: '{{ tooling_environment }}'
      tooling_placement: '{{ item.1.availability_zone }}'
      stack: '{{ application_stack }}'
      role: '{{ ec2_role_name }}'
      identifier: '{{ ec2_role_name }}'
      network-position: Internal
    exact_count: '{{ servers.instance_count }}'
    count_tag:
      Name: '{{ ec2_server_name }}'
      tooling_environment: '{{ tooling_environment }}'
      tooling_placement: '{{ item.1.availability_zone }}'
      stack: '{{ application_stack }}'
      role: '{{ ec2_role_name }}'
      identifier: '{{ ec2_role_name }}'
      network-position: Internal
    zone: '{{ item.1.availability_zone }}'
  with_subelements:
    - '{{ aws_subnets.results}}'
    - subnets
  register: ec2
  when: aws_vpc_id is defined

- debug:
    var: ec2
    verbosity: 1

- set_fact:
    ec2_instance_ids: "{{ ec2.results | map(attribute='tagged_instances') | map('first') | map(attribute='id') | list }}"

- ec2_vol_facts:
    region: '{{ aws_region }}'
    filters:
      attachment.instance-id: '{{ item }}'
  with_items: '{{ ec2_instance_ids }}'
  register: ec2_vol_facts

- set_fact:
    ec2_vol_ids: "{{ ec2_vol_facts.results | map(attribute='volumes') | map('first') | map(attribute='id') | list }}"

- name: Ensure all volumes are tagged
  ec2_tag:
    region: '{{ aws_region }}'
    resource: '{{ item }}'
    state: present
    tags:
      dataclassification: Public
  with_items: '{{ ec2_vol_ids }}'


- name: Set default username for HSBC RHEL ami
  set_fact:
    image_default_username: ec2-user


- name: Register Hosts for the Play
  add_host:
    name: '{{ item.1.private_ip }}'
    instance_id: '{{ item.1.id }}'
    aws_region: '{{ aws_region }}'
    ansible_user: '{{ image_default_username }}'
    remote_user: '{{ image_default_username }}'
    private_dns_name: '{{ item.1.private_dns_name }}'
    groups: prisma_servers,tag_roles_{{ ec2_role_name | regex_replace('-', '_') }},tag_stack_{{ application_stack }}
  with_subelements:
    - '{{ ec2.results }}'
    - tagged_instances
  when: aws_vpc_id is defined

- set_fact:
    instance_ids: "{{ ec2.results | map(attribute='tagged_instances') | map('first') | map(attribute='id') | list }}"

- name: Wait for Servers before adding them to the ALB
  wait_for_connection:
    delay: 45
    sleep: 15
    timeout: 500
  delegate_to: "{{ item }}"
  with_items: "{{ groups['prisma_servers'] }}"

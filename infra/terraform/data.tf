
data "aws_ami" "prisma" {
  most_recent = true

  filter {
    name   = "name"
    values = ["prisma-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["555137535837"]
}

data "aws_vpc" "prisma" {
  tags ={
    name   = "prisma_vpc"
  }
}

data "aws_subnet_ids" "prisma" {
  vpc_id = data.aws_vpc.prisma.id
}
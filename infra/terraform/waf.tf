resource "aws_wafv2_web_acl" "prisma" {
  name        = "AWS-AWSManagedRulesAmazonIpReputationList"
  scope       = "CLOUDFRONT"

  default_action {
    allow {}
  }

  rule {
    name     = "rule-1"
    priority = 1

    override_action {
      count {}
    }

    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesCommonRuleSet"
        vendor_name = "AWS"
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = false
      metric_name                = "AWS-AWSManagedRulesKnownBadInputsRuleSet"
      sampled_requests_enabled   = true
    }

  }
  tags = local.core_tags
  visibility_config {
    cloudwatch_metrics_enabled = false
    metric_name                = "prisma-metric"
    sampled_requests_enabled   = false
  }
}

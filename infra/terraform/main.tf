# Create a new instance of the latest Ubuntu 20.04 on an
# t3.micro node with an AWS Tag naming it "HelloWorld"
provider "aws" {
  region = "us-east-1"
}

locals {
  env = terraform.workspace
  core_tags = {
    "ProductName"       = "Events Queue",
    "Environment"       = "${terraform.workspace}",
    "ProductOwner"      = "usw",
    "ContactEMail"      = "nathan.hampshire@gmail.com",
    "AppName"           = "prisma"
    "ControlledBy"      = "terraform"
    }
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = data.aws_vpc.prisma.id

  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "web" {
  ami           = data.aws_ami.prisma.id
  instance_type = "t2.micro"
  tags          = local.core_tags
  security_groups = [aws_security_group.allow_tls.name]
  user_data     = <<EOF
#!/bin/bash -xe 
su ec2-user -c 'cd /home/ec2-user/prisma;curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash; . ~/.nvm/nvm.sh;nvm install node;npm install next;npm run build;npm run start'
EOF
}

resource "aws_lb" "prisma" {
  name               = "prisma-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_tls.id]
  subnets            = data.aws_subnet_ids.prisma.ids

  enable_deletion_protection = false

  tags = local.core_tags

}

resource "aws_lb_target_group" "prisma" {
  name     = "prisma-lb-tg"
  port     = 3000
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.prisma.id
}

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_lb_target_group.prisma.arn
  target_id        = aws_instance.web.id
  port             = 3000
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.prisma.arn
  port              = "3000"
  protocol          = "HTTP"
#  ssl_policy        = "ELBSecurityPolicy-2016-08"
#  certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.prisma.arn
  }
}


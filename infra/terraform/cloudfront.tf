resource "aws_cloudfront_distribution" "elb_distribution" {
  origin {
    domain_name              = aws_lb.prisma.dns_name
    origin_id                = aws_lb.prisma.id
    custom_origin_config {
      http_port              = "3000"
      https_port             = "3000"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }
  web_acl_id                 = aws_wafv2_web_acl.prisma.arn

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_lb.prisma.id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }
    restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["US", "CA", "GB", "DE"]
    }
  }
  viewer_certificate {    
      cloudfront_default_certificate = true
}
  enabled             = true
  tags                = local.core_tags
}
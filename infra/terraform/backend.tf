terraform {
  required_version = ">= 0.12.25"
  backend "s3" {
    bucket         = "prismacollection"
    key            = "terraform.tfstate"
    region         = "ap-southeast-2"
  }
}
pipeline {
    agent any
    
    stages {
        stage("Build infrastructure (Ansible)") {
            steps {
                dir('') {
                 git url: 'https://gitlab.com/NathanH05/prisma.git'
            
                 ansiblePlaybook([
                  colorized: true,
                  credentialsId: '1',
                  installation: 'ansible',
                  playbook: 'infra/prisma.yml',
                  extras: "-vv --extra-vars 'tooling_environment=dev aws_region=us-east-1 application_stack=blue app_role_name=prisma'"
                    ])
                }
            }
        }

    }
}
